import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MorePage } from '../pages/more/more';
import { ActionmorePage } from '../pages/actionmore/actionmore';
import { AdventuremorePage } from '../pages/adventuremore/adventuremore';
import { ArcademorePage } from '../pages/arcademore/arcademore';
import { CasualmorePage } from '../pages/casualmore/casualmore';
import { CricketmorePage } from '../pages/cricketmore/cricketmore';
import { PuzzlemorePage } from '../pages/puzzlemore/puzzlemore';
import { RacingmorePage } from '../pages/racingmore/racingmore';
import { SportsmorePage } from '../pages/sportsmore/sportsmore';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    MorePage,ActionmorePage,AdventuremorePage,ArcademorePage,CasualmorePage,CricketmorePage,PuzzlemorePage,RacingmorePage,SportsmorePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    MorePage,ActionmorePage,AdventuremorePage,ArcademorePage,CasualmorePage,CricketmorePage,PuzzlemorePage,RacingmorePage,SportsmorePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
