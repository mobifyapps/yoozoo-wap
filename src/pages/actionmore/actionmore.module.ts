import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActionmorePage } from './actionmore';

@NgModule({
  declarations: [
    ActionmorePage,
  ],
  imports: [
    IonicPageModule.forChild(ActionmorePage),
  ],
})
export class ActionmorePageModule {}
