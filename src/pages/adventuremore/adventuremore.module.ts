import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdventuremorePage } from './adventuremore';

@NgModule({
  declarations: [
    AdventuremorePage,
  ],
  imports: [
    IonicPageModule.forChild(AdventuremorePage),
  ],
})
export class AdventuremorePageModule {}
