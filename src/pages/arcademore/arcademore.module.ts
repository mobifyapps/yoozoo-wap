import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArcademorePage } from './arcademore';

@NgModule({
  declarations: [
    ArcademorePage,
  ],
  imports: [
    IonicPageModule.forChild(ArcademorePage),
  ],
})
export class ArcademorePageModule {}
