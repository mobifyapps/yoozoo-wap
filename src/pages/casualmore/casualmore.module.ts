import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CasualmorePage } from './casualmore';

@NgModule({
  declarations: [
    CasualmorePage,
  ],
  imports: [
    IonicPageModule.forChild(CasualmorePage),
  ],
})
export class CasualmorePageModule {}
