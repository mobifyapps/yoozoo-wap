import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CricketmorePage } from './cricketmore';

@NgModule({
  declarations: [
    CricketmorePage,
  ],
  imports: [
    IonicPageModule.forChild(CricketmorePage),
  ],
})
export class CricketmorePageModule {}
