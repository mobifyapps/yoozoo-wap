import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MorePage } from '../more/more';
import { ActionmorePage } from '../actionmore/actionmore';
import { AdventuremorePage } from '../adventuremore/adventuremore';
import { ArcademorePage } from '../arcademore/arcademore';
import { CasualmorePage } from '../casualmore/casualmore';
import { CricketmorePage } from '../cricketmore/cricketmore';
import { PuzzlemorePage } from '../puzzlemore/puzzlemore';
import { RacingmorePage } from '../racingmore/racingmore';
import { SportsmorePage } from '../sportsmore/sportsmore';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  // styleUrls :[ 'home.scss']
})
export class HomePage {

  morePage = MorePage;
  actionpage = ActionmorePage;
  adventurepage = AdventuremorePage;
  arcadepage = ArcademorePage;
  casualpage = CasualmorePage;
  criketpage = CricketmorePage;
  puzzlemore = PuzzlemorePage;
  racingmore = RacingmorePage;
  sportsmore = SportsmorePage;
  
  slideData: Array<{image: string}>;

  constructor(public navCtrl: NavController) {
   this.slideData = [{ image: "../../assets/imgs/slide-4.jpg" },{ image: "../../assets/imgs/slide-2.jpg" },{ image: "../../assets/imgs/slide-5.jpg" }]
  }

}
