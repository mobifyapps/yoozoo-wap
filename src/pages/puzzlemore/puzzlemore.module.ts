import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PuzzlemorePage } from './puzzlemore';

@NgModule({
  declarations: [
    PuzzlemorePage,
  ],
  imports: [
    IonicPageModule.forChild(PuzzlemorePage),
  ],
})
export class PuzzlemorePageModule {}
