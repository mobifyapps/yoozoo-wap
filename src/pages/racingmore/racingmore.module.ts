import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RacingmorePage } from './racingmore';

@NgModule({
  declarations: [
    RacingmorePage,
  ],
  imports: [
    IonicPageModule.forChild(RacingmorePage),
  ],
})
export class RacingmorePageModule {}
