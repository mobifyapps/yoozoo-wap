import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportsmorePage } from './sportsmore';

@NgModule({
  declarations: [
    SportsmorePage,
  ],
  imports: [
    IonicPageModule.forChild(SportsmorePage),
  ],
})
export class SportsmorePageModule {}
